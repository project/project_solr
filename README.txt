This module integrates Project with the ApacheSolr integration module
(http://drupal.org/project/apachesolr).  It provides a set of project
browsing pages based on ApacheSolr instead of Views.  It is used to
power the drupal.org project browsing pages, for example:

http://drupal.org/project/modules
